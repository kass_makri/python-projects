""" Case study with air pollution data found here http://archive.ics.uci.edu/ml/datasets/Air+quality.

The case study consists of the following questions:

Q1: What is the maximum CO concentration recorded during May 2004?
Q2: Looking at the highest 10 recordings of NOX, what was the most common hour of the day and day of the week?
Q3: What is the total NO2 recorded in Jan 2005
Q4: What month and year was the lowesst average absolute humidity found in?
Q5: What month and year was the highest variability in temperature found in?
 """


import numpy as np
import pandas as pd 

pollution_df=pd.read_csv(r'/Users/kassandramakri/Documents/Past_Projects/python-projects-1/Air pollution_case study/AirQualityUCI.csv',sep=";",thousands=",") # read csv file and save in a dataframe

# EDA on dataset to look for outliers, duplicates etc.

pollution_df.describe()
pollution_df.dtypes
pollution_df.isna().sum()

# the last two columns have no data - drop both
pollution_df.drop(['Unnamed: 15','Unnamed: 16'],1,inplace=True)

# there are only 114 missing values so it is safe to drop them by row
pollution_df=pollution_df.dropna(axis=0,how='any')

#Convert Date and Time columns into datetime series fro easier manipulation
pollution_df['Time']=pollution_df['Time'].str.replace(".",":")
pollution_df['Time']=pd.to_datetime(pollution_df['Time'])
pollution_df['Date']=pd.to_datetime(pollution_df['Date'])

# check for outliers and replace with median
median=pollution_df.median()
std=pollution_df.std()
outliers= (pollution_df.select_dtypes('float')-median).abs()> 2*std
pollution_df[outliers]=np.nan
pollution_df.fillna(median,inplace=True)

# Q1
max_CO=pollution_df[(pollution_df['Date'].dt.month==5) & (pollution_df['Date'].dt.year==2004)]['CO(GT)'].max()

print('The maximum CO concentration recorded during May 2004 is:', round(max_CO))

#Q2 
Top_10_NOx=pollution_df[['Date','Time','NOx(GT)']].sort_values(by='NOx(GT)',ascending=False)[:10]
Top_10_NOx['Weekday']=Top_10_NOx['Date'].dt.weekday
Most_freq_day=Top_10_NOx['Weekday'].value_counts().idxmax()
Most_freq_hour=Top_10_NOx['Time'].value_counts().idxmax().time()
weekdays=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']

print ("Looking at the top 10 recordings of NOx, ",Most_freq_hour,"hour of the day and",weekdays[Most_freq_day],"as the day of the week was most common")

#Q3
max_NO2= pollution_df[(pollution_df['Date'].dt.month==1) & (pollution_df['Date'].dt.year==2005)]['NO2(GT)'].sum()

print(round(max_NO2),"was the total NO2 recorded in January 2005")

#Q4
pollution_df['Month_date']=pollution_df['Date'].dt.month
pollution_df['Year_date']=pollution_df['Date'].dt.year
min_avg_hum=pollution_df.groupby(['Month_date','Year_date'])['AH'].mean().idxmin()

print("Month",round(min_avg_hum[0]),"of the",round(min_avg_hum[1]),"year had the lowest average absolute humidity")

# Q5
diff_temp=pollution_df.groupby(['Month_date','Year_date'])['T'].max()-pollution_df.groupby(['Month_date','Year_date'])['T'].min()
max_diff=diff_temp.idxmax()

print("Month",round(max_diff[0]),"of the",round(max_diff[1]),"year had the highest variability in temperature")

