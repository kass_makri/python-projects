""" Case study with air pollution data found here http://archive.ics.uci.edu/ml/datasets/Air+quality.

The objective is to build a linear model to predict CO concentration one our ahead and compare it to a simple benchmark.

Q1 Construct a new column called "target" which is the CO concentration lagged forward by one timestep. Calculate the pearson correlation matrix for all variables. 
    Which 5 variables are most correlated with "target" (excl. target) from highest to lowest?

Q2 Use these variables along with the CO concentration) as inputs for a linear model predicting the Co concentration one hour ahead. 
    Calculate the mean absolute error, root mean squared error and coefficient of determination of your model.

 """


import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

pollution_df=pd.read_csv(r'/Users/kassandramakri/Documents/Past_Projects/python-projects-1/Air pollution_case study/AirQualityUCI.csv',sep=";",thousands=",") # read csv file and save in a dataframe


# EDA on dataset to look for outliers etc.

pollution_df.describe()
pollution_df.dtypes
pollution_df.isna().sum()

# the last two columns have no data - drop both
pollution_df.drop(['Unnamed: 15','Unnamed: 16'],1,inplace=True)

# there are only 114 missing values so it is safe to drop them by row
pollution_df=pollution_df.dropna(axis=0,how='any')

#Convert Date and Time columns into datetime series fro easier manipulation
pollution_df['Time']=pollution_df['Time'].str.replace(".",":")
pollution_df['Time']=pd.to_datetime(pollution_df['Time'])
pollution_df['Date']=pd.to_datetime(pollution_df['Date'])

# check for outliers and replace with median
median=pollution_df.median()
std=pollution_df.std()
outliers= (pollution_df.loc[:,'CO(GT)':'AH']-median).abs()> 2*std
pollution_df[outliers]=np.nan
pollution_df.fillna(median,inplace=True)

#Q1
pollution_df['target']=pollution_df['CO(GT)'].shift(periods=1)

cor=pollution_df.corr().abs()['target'].sort_values(ascending=False)[2:7].index

print("The 5 variables which are the most correlated to the target variable from highest to lower are:",cor[0],",",cor[1],",",cor[2],",",cor[3],",",cor[4])

#Q2
#Check for missing values
pollution_df.isna().sum()
pollution_df=pollution_df.dropna(axis=0,how='any')


X=pollution_df[[cor[0],cor[1],cor[2],cor[3],cor[4]]]
y=pollution_df['target']

X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.4,random_state=42)

regr=LinearRegression()
regr.fit(X_train,y_train)

y_pred=regr.predict(X_test)

r2Score=r2_score(y_test,y_pred)
MSE=mean_squared_error(y_test,y_pred)
MAE=mean_absolute_error(y_test,y_pred)

print("The mean absolute error, the mean squared error and the coefficient of determination of the model are: {:.2f}, {:.2f}, {:.2f}".format( MAE,MSE,r2Score),"respectively")


