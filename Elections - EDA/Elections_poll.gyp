"""This is a Data Science Interview Qustion fro, TestDome  available at : https://www.testdome.com/d/data-science-interview-questions/839
Description of the task:
Each day during 2019 an agency asked a hundred randomly selected people which party they would vote for if elections were held that day. Results of the poll were recorded in the following file. The Workers' Party asked for the report which they plan to use to improve their strategy for upcoming elections.

#Fill in the missing values in the report for 2019:

#The arithmetic mean of votes for the Workers' Party is: ▲▼ (rounded to one decimal place)
#The median of votes for the Workers' party is: ▲▼ (rounded to closest integer)
#The standard deviation of votes for the Workers' party is: ▲▼ (rounded to one decimal place)
#The difference between the largest and the smallest number of votes for the Workers' party for March is: ▲▼
#The largest number of votes that any party received on any day is: ▲▼ votes.
#That maximum was achieved on 2019-▲▼-▲▼ by .
#The party with the largest difference between the maximum and minimum number of votes is . That difference is ▲▼ votes."""



import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# load the csv file and save in a dataframe
poll_df=pd.read_csv(r'/Users/kassandramakri/Documents/Past_Projects/python-projects-1/Elections/electionpoll.csv')

# Fix the header names
poll_df.columns=poll_df.columns.str.replace("'","")

# check worker's party data for outliers
sns.boxplot(poll_df['Workers Party'])
plt.show()  # the figure shows that there are a few potential outliers

# if data<Q1-1.5*IQR then outliers

IQR=np.quantile(poll_df['Workers Party'],0.75)-np.quantile(poll_df['Workers Party'],0.25)
outliers=poll_df['Workers Party'][(poll_df['Workers Party']< (np.quantile(poll_df['Workers Party'],0.25)-1.5*IQR)) | (poll_df['Workers Party']> (np.quantile(poll_df['Workers Party'],0.75)+1.5*IQR))]

print(" The mean of votes for the worker's party is: {:.1f}" .format(poll_df['Workers Party'].drop(outliers).mean())) # mean of clean dataset
print(" The median of votes dfor the worker's party is: {:.1f}" .format(poll_df['Workers Party'].drop(outliers).median())) # median of clean dataset
print(" The standard deviation of votes dfor the worker's party is: {:.1f}" .format(poll_df['Workers Party'].drop(outliers).std())) # std of clean dataset

poll_df['Date']=pd.to_datetime(poll_df['Date \ Party']) # convert data dat to datetime for easier manipulation

March_data= poll_df[poll_df['Date'].dt.month==3]['Workers Party'].drop(outliers)

print("The difference between the largest and smallest number of voters for the Worker's party in March is: {:.0f}" .format(March_data.max()-March_data.min()))

# check outliers across all party data
Max_votes=[]
Difference=[]
for column in range(1,poll_df.shape[1]-1):
    IQR=np.quantile(poll_df.iloc[:,column],0.75)-np.quantile(poll_df.iloc[:,column],0.25)
    outliers=poll_df.iloc[:,column][(poll_df.iloc[:,column]<np.quantile(poll_df.iloc[:,column],0.25)-1.5*IQR)|(poll_df.iloc[:,column]>np.quantile(poll_df.iloc[:,column],0.75)+1.5*IQR)]
    max_vote=poll_df.iloc[:,column].drop(outliers).max() # max value after removing outliers
    Max_votes.append(max_vote)
    diff=poll_df.iloc[:,column].drop(outliers).max() -poll_df.iloc[:,column].drop(outliers).min() 
    Difference.append(diff)

print( "The largest number of votes that any party received on any day is",Party_max_votes)   

parties=poll_df.columns[1:-1]
Party_max_votes=parties[Max_votes.index(max(Max_votes))]
date_max=poll_df[poll_df[parties[Max_votes.index(max(Max_votes))]]==np.max(Max_votes)]['Date']

print(" The party achieved the largest numner of votes was:",Party_max_votes)

party_max_diff=parties[Difference.index(max(Difference))]

print("The party with the largest difference between the maximum and minimum number of votes is the", party_max_diff)

print(" The largest difference is:", np.max(Difference))