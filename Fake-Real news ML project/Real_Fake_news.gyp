import pandas as pd
import numpy as np
import itertools
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import accuracy_score,confusion_matrix

df=pd.read_csv(r'/Users/kassandramakri/Documents/Python Projects/Fake-Real news ML project/news.csv')

df.head()
df.dtypes # check that the datatypes match the variable description
df.isna().sum() # Check if there are null values in the dataset

label=df.label
label.head()

# split the dataset
x_train,x_test,y_train,y_test=train_test_split(df['text'],label,test_size=0.2,random_state=7)

# Initialize a TfidfVectoriser to filter out the most common words before processing
tfidf_vectorizer=TfidfVectorizer(stop_words='english',max_df=0.7)

# Fit and tranform the training data, transform test data
tfidf_train=tfidf_vectorizer.fit_transform(x_train)
tfidf_test=tfidf_vectorizer.transform(x_test)

# Initialise a PassiveAgressiveClassifier
pac=PassiveAggressiveClassifier(max_iter=50)
pac.fit(tfidf_train,y_train)

# Predict on test and check accuracy
y_pred=pac.predict(tfidf_test)
score=accuracy_score(y_test,y_pred)
print(f'Accuracy: {round(score*100,2)}%')

confusion_matrix(y_test,y_pred,labels=['FAKE','REAL'])